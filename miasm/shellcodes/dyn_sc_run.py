import os
from pdb import pm
from miasm2.analysis.sandbox import Sandbox_Win_x86_32
from miasm2.jitter.csts import  PAGE_READ, PAGE_WRITE
from miasm2.os_dep.common import set_str_unic

# Python auto completion
filename = os.environ.get('PYTHONSTARTUP')
if filename and os.path.isfile(filename):
    execfile(filename)

# Insert here user defined methods

def ole32_CoInitializeEx(jitter):
    ret_ad, args = jitter.func_args_stdcall(["pvReserved", "dwCoInit"])
    jitter.func_ret_stdcall(ret_ad, 1)


def ntdll_swprintf(jitter):
    ret_ad, args = jitter.func_args_stdcall(["dst", "pfmt"])
    fmt = jitter.get_str_unic(args.pfmt)
    print "FMT:", repr(fmt)
    if fmt == "%S":
        psrc = jitter.pop_uint32_t()
        src = jitter.get_str_ansi(psrc)
        out = "%s" % src
    else:
        raise RuntimeError("unknown fmt %s" % fmt)
    print "OUT:", repr(out)
    jitter.set_str_unic(args.dst, out)

    # Returns the string len in wchar unit
    jitter.func_ret_stdcall(ret_ad, len(out)/2)

def urlmon_URLDownloadToCacheFileW(jitter):
    ret_ad, args = jitter.func_args_stdcall(["lpunkcaller",
                                             "szurl",
                                             "szfilename",
                                             "ccfilename",
                                             "reserved",
                                             "pbsc"])
    url = jitter.get_str_unic(args.szurl)
    print "URL:", url
    jitter.set_str_unic(args.szfilename, "toto")
    jitter.func_ret_stdcall(ret_ad, 0)

def kernel32_myCreateProcess(jitter, func):
    ret_ad, args = jitter.func_args_stdcall(["lpappname",
                                             "lpcmdline",
                                             "lpprocessattr",
                                             "lpthreadattr",
                                             "binherithandles",
                                             "createflags",
                                             "lpenv",
                                             "lpcurdir",
                                             "lpstartinfo",
                                             "lpprocessinfo"])

    appname, cmdline = None, None
    if args.lpappname:
        appname = func(args.lpappname)
    if args.lpcmdline:
        cmdline = func(args.lpcmdline)

    print "Run:", repr(appname), repr(cmdline)
    jitter.func_ret_stdcall(ret_ad, 1)


def kernel32_CreateProcessA(jitter):
    kernel32_myCreateProcess(jitter, jitter.get_str_ansi)

def kernel32_CreateProcessW(jitter):
    kernel32_myCreateProcess(jitter, jitter.get_str_unic)

# Parse arguments
parser = Sandbox_Win_x86_32.parser(description="PE sandboxer")
parser.add_argument("filename", help="PE Filename")
parser.add_argument("shellcode", help="shellcode file")
options = parser.parse_args()

# Create sandbox
sb = Sandbox_Win_x86_32(options.filename, options, globals())

# Read and map the shellcode
data = open(options.shellcode).read()
run_addr = 0x40000000
sb.jitter.vm.add_memory_page(run_addr, PAGE_READ | PAGE_WRITE, data)

# Place EAX to the shellcode location
sb.jitter.cpu.EAX = run_addr

def stop_exec(jitter):
    return False

sb.jitter.ir_arch.symbol_pool.add_label('resolve_by_hash', 0x400002ca)

# Run the shellcode
sb.run(run_addr)

