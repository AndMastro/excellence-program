Steps:

0) install multilib to enable x86_32 bit compilation, since miasm uses 32 bit libraries

1) Disassembly shellcode1.bin using the following command line:
     python miasm/example/disasm/full.py -m x86_32 shellcode1.bin --blockwatchdog 1

2) Obtain graph_execflow.dot

3) Run command line:
     dot graph_execflow.dot -Tpng -o graph_execflow.png

4) Obtain graph_execflow.png (graphviz must be installed to do so)

5) Check graph_execflow.png to spot the istruction location you are interested in. In this case we focus on observing which value the register ECX will get
    at that specific point of the execution.
    
6) In the prova_shellcode1.py set the disassembly breakpoint at the location mentioned above (in this case 0x1C)

7) Run command line:
     python prova_shellcode1.py shellcode1.bin

8) Obtain ir_graph.dot and generate ir_graph.png as in step 3 (this is IR language, an intermediate language used by miasm)

9) Check terminal output, in this case ECX value:

ECX = (EAX_init+0xFFFFFFF0)