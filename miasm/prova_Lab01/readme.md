For this run we use files taken from the book http://venom630.free.fr/pdf/Practical_Malware_Analysis.pdf

Files link: https://www.dropbox.com/s/mfle4cp59iq182a/BinaryCollection.tar.gz

Steps:

0) install multilib to enable x86_32 bit compilation, since miasm uses 32 bit libraries

1) Disassembly shellcode1.bin using the following command line: python miasm/example/disasm/full.py -m x86_32 Lab01-01.exe --blockwatchdog 1

2) Obtain graph_execflow.dot

3) Run command line: dot graph_execflow.dot -Tpng -o graph_execflow.png

4) Obtain graph_execflow.png (graphviz must be installed to do so)

5) Check graph_execflow.png to spot the istruction location you are interested in. In this case we focus on observing which value the register ESP will get at that specific point of the execution.

6) In the prova_Lab01.py set the disassembly breakpoint at the location mentioned above (in this case 0x0040184C)

7) Run command line: python prova_Lab01.py Lab01-01.exe

8) Obtain ir_graph.dot and generate ir_graph.png as in step 3 (this is IR language, an intermediate language used by miasm)

9) Check terminal output, in this case ESP value

Observations (and issues):
We got the following terminal output: 

WARNING: cannor disasm (guess) at C

WARNING: cannot disasm at C

ESP = (ESP_init+0x4)


It is probably referred to the breakpoint address defined at step 5. So we can't assume that the output is reliable.
