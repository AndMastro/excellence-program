.globl f

f:
	movl	4(%esp),%ecx		#ecx = 10
	movl	$0,		%eax		#eax = 0
	cmpl	$1,		%ecx		#ecx - 1
	je		E1					#ecx = 1
	cmpl	$2,		%ecx		#ecx - 2
	je		E2					#ecx = 2
	cmpl	$3,		%ecx		#ecx - 3
	je		E3					#ecx = 3
	jmp END
E1:
	movl	$10,	%eax		#eax = 1
	jmp END
E2:
	movl	$20,	%eax		#eax = 1
	jmp END
E3:
	movl	$30,	%eax		#eax = 1
	jmp END
END:
	ret
