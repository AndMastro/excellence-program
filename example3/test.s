.globl f

f:
	movl	4(%esp),%ecx		#ecx = 1
	movl	$0,		%eax		#eax = 0
E:
	incl	%eax
	cmpl	%eax,	%ecx		#ecx - eax
	jne		E					#ecx != eax
	ret
