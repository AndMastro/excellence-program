import sys

from miasm2.analysis.machine import Machine
from miasm2.core.bin_stream import bin_stream_str
from miasm2.ir.symbexec import symbexec

# Create a bin_stream from a Python string
bs = bin_stream_str(open(sys.argv[1]).read())

# Get a Miasm x86 32bit machine
machine = Machine("x86_32")
# Retrieve the disassemble and IR analysis
dis_engine, ira = machine.dis_engine, machine.ira

# link the disasm engine to the bin_stream
mdis = dis_engine(bs)

# Address where to stop
mdis.dont_dis = [0x41f]
# Disassemble blocks starting from this address
blocks = mdis.dis_multibloc(0x409)

# instanciate an IR analysis
ir_arch = ira(mdis.symbol_pool)
# Translate asm basic blocks to IR basic blocks
for block in blocks: ir_arch.add_bloc(block)

# Store IR graph
open('ir_graph.dot', 'w').write(ir_arch.graph.dot())

# Initiate the symbolic execution engine
# regs_init associates EAX to EAX_init and to on
sb = symbexec(ir_arch, machine.mn.regs.regs_init)
# Start execution at address 0
# IRDst represents the label of the next IR basic block to execute
irdst = sb.emul_ir_blocs(ir_arch, 0)

print 'EAX =', sb.symbols[machine.mn.regs.EAX]
