.globl f

f:
	movl	4(%esp),%ecx		#ecx = 1
	movl	$0,		%eax		#eax = 0
E:	
	incl	%eax
	cmpl	$1,		%ecx		#ecx - 1
	je		E					#ecx = 1
	ret
