.globl f

f:
	movl	4(%esp),%ecx			#ecx = 1
	movl	$0,		%eax		#eax = 0
	cmpl	$1,		%ecx		#ecx - 1
	je	END				#ecx = 1
	call	foo
END:	
	ret
